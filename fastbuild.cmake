macro(fb_compiler_cxx)
  set(FB_COMPILER_PATH ".Compiler\t\t\t=\t\"${CMAKE_CXX_COMPILER}\"")
endmacro()

macro(fb_compiler_c)
  set(FB_COMPILER_PATH ".Compiler\t\t\t=\t\"${CMAKE_C_COMPILER}\"")
endmacro()

macro(fb_linker)
  set(FB_LINKER_PATH ".Linker\t\t\t=\t\"${CMAKE_LINKER}\"")
endmacro()

# fb_add_compiler_option() adds options to the .CompilerOptions field.
macro(fb_add_compiler_option FB_NEW_COMPILER_OPTION)
  if("${FB_COMPILER_OPTIONS}" STREQUAL "")
    set(FB_COMPILER_OPTIONS ".CompilerOptions\t=\t${FB_NEW_COMPILER_OPTION}\n")
  else()
    set(FB_COMPILER_OPTIONS "${FB_COMPILER_OPTIONS}\t\t\t\t\t+\t${FB_NEW_COMPILER_OPTION}\n")
  endif()
endmacro()

# fb_add_linker_option() adds options ot the .LinkerOptions field.
macro(fb_add_link_option FB_NEW_LINKER_OPTION)
  if("${FB_LINKER_OPTIONS}" STREQUAL "")
    set(FB_LINKER_OPTIONS ".LinkerOptions\t=\t${FB_NEW_LINKER_OPTION}\n")
  else()
    set(FB_LINKER_OPTIONS "${FB_LINKER_OPTIONS}\t\t\t\t+\t${FB_NEW_LINKER_OPTION}\n")
  endif()
endmacro()

macro(fb_add_lib_path FB_NEW_LIB_PATH)
  if("${FB_LIB_PATHS}" STREQUAL "")
    set(FB_LIB_PATHS ".LibPaths\t=\t\"${FB_NEW_LIB_PATH}\"\n")
  else()
    set(FB_LIB_PATHS "${FB_LIB_PATHS}\t\t\t+\t\"${FB_NEW_LIB_PATH}\"\n")
  endif()
endmacro()

macro(fb_link_directories)
  get_property(fb_tmp_LINK_DIRECTORIES DIRECTORY PROPERTY LINK_DIRECTORIES)
  foreach(DIR ${fb_tmp_LINK_DIRECTORIES})
    fb_add_lib_Path(${DIR})
  endforeach()
endmacro()

macro(fb_compiler_options)
  get_property(fb_tmp_COMPILER_OPTIONS DIRECTORY PROPERTY COMPILE_OPTIONS)
  foreach(OPT ${fb_tmp_COMPILER_OPTIONS})
    fb_add_compiler_option(${OPT})
  endforeach()
endmacro()

macro(fb_link_options)
  get_property(fb_tmp_LINK_OPTIONS DIRECTORY PROPERTY LINK_OPTIONS)
  foreach(OPT ${fb_tmp_LINK_OPTIONS})
    fb_add_link_option(${OPT})
  endforeach()
endmacro()

macro(fb_link_libraries)
  get_property(fb_tmp_LINK_LIBRARIES DIRECTORY PROPERTY LINK_LIBRARIES)
  set(FB_LIBRARIES_LIST ".Libraries = { ")
  foreach(LIB ${fb_tmp_LINK_LIBRARIES})
    set(FB_LIBRARIES_LIST "${FB_LIBRARIES_LIST}\n\t\t'${LIB}'")
  endforeach()
  set(FB_LIBRARIES_LIST "${FB_LIBRARIES_LIST}\n\t}")
endmacro()

macro(fb_executable)
  fb_link_libraries()
  set(FB_EXECUTABLE_DATA "Executable( '${PROJECT_NAME}' )\n{\n\t")
  set(FB_EXECUTABLE_DATA "${FB_EXECUTABLE_DATA}${FB_LIBRARIES_LIST}\n")

  get_property(fb_tmp_LINK_OUTPUT DIRECTORY PROPERTY OUTPUT_NAME)
  set(FB_EXECUTABLE_DATA "${FB_EXECUTABLE_DATA}\t.LinkerOutput = '${RUNTIME_OUTPUT_NAME}'\n}")
endmacro()

macro(dump_to_bff FB_FILE_PATH)
  fb_compiler_options()
  fb_link_options()
  fb_link_directories()

  fb_executable()

  file(
    WRITE
    ${FB_FILE_PATH}
    "//------------------------------------------------------------------------------\n"
    "// Compiler\n"
    "//------------------------------------------------------------------------------\n"
    "${FB_COMPILER_PATH}\n"
    "${FB_COMPILER_OPTIONS}\n\n"
    "${FB_LINKER_PATH}\n"
    "${FB_LINKER_OPTIONS}\n\n"
    "${FB_LIB_PATHS}\n"

    "${FB_EXECUTABLE_DATA}"
    )
endmacro()
